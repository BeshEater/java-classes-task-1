package com.besheater.training.javaclasses.task1;

import com.besheater.training.javaclasses.task1.entity.Candy;
import com.besheater.training.javaclasses.task1.entity.CandyBag;
import com.besheater.training.javaclasses.task1.repository.CandyRepository;

import java.io.PrintStream;
import java.io.UnsupportedEncodingException;

public class App {
    private static final CandyRepository candyRepository = new CandyRepository();

    public static void main(String[] args) throws UnsupportedEncodingException {
        // Use UTF-8 encoding for output to correctly display tenge symbol (₸)
        PrintStream out = new PrintStream(System.out, true, "UTF-8");

        // Create candy bag and fill it with all available candies (let's be generous).
        CandyBag candyBag = new CandyBag();
        candyBag.add(candyRepository.getAll());
        out.println("Candy bag content:");
        out.println(candyBag.printContent());

        // Find candy bag price and weight
        out.printf("Total candy bag price = %d ₸\n", candyBag.getPrice());
        out.printf("Total candy bag weight = %d g\n", candyBag.getWeight());

        // Sort candy bag by weight
        out.print("\n");
        out.println("Candy bag sorted by weight:");
        candyBag.sort(Candy::compareByWeight);
        out.println(candyBag.printContent());

        // Find candy that has sugar content between 40% and 45%
        out.print("\n");
        Candy notSoSweetCandy = candyBag.getAll().stream()
                                                 .filter(c -> c.isSugarContentInRange(0.4, 0.45))
                                                 .findFirst()
                                                 .get();
        out.println("Candy with sugar content between 40% and 45% = " + notSoSweetCandy.getName());
    }
}