package com.besheater.training.javaclasses.task1.entity;

import java.util.Objects;

public class Candy {
    public static final double MIN_SUGAR_CONTENT = 0;
    public static final double MAX_SUGAR_CONTENT = 1;

    private final String name;
    private final long price;
    private final long weight;
    private final double sugarContent;

    public Candy(String name, long price, long weight, double sugarContent) {
        checkName(name);
        checkPrice(price);
        checkWeight(weight);
        checkSugarContent(sugarContent);
        this.name = name;
        this.price = price;
        this.weight = weight;
        this.sugarContent = sugarContent;
    }

    private void checkName(String name) {
        if (name == null) {
            throw new IllegalArgumentException("Name cannot be null");
        }
        if (name.equals("")) {
            throw new IllegalArgumentException("Name cannot be empty");
        }
    }

    private void checkPrice(long price) {
        if (price < 0) {
            throw new IllegalArgumentException("Price cannot be less than 0");
        }
    }

    private void checkWeight(long weight) {
        if (weight < 0) {
            throw new IllegalArgumentException("Weight cannot be less than 0");
        }
    }

    private void checkSugarContent(double sugarContent) {
        if (sugarContent < MIN_SUGAR_CONTENT || sugarContent > MAX_SUGAR_CONTENT) {
            String message = String.format("Sugar content is not in range. " +
                                           "It should be between %.0f and %.0f inclusive",
                                            MIN_SUGAR_CONTENT, MAX_SUGAR_CONTENT);
            throw new IllegalArgumentException(message);
        }
    }

    public String getName() {
        return name;
    }

    public long getPrice() {
        return price;
    }

    public long getWeight() {
        return weight;
    }

    public double getSugarContent() {
        return sugarContent;
    }

    public boolean isSugarContentInRange(double from, double to) {
        checkSugarContent(from);
        checkSugarContent(to);
        if (to < from) {
            throw new IllegalArgumentException("'To' cannot be less than 'from'");
        }
        return sugarContent >= from && sugarContent <= to;
    }

    public static int compareByName(Candy firstCandy, Candy secondCandy) {
        return firstCandy.name.compareTo(secondCandy.name);
    }

    public static int compareByPrice(Candy firstCandy, Candy secondCandy) {
        return Long.compare(firstCandy.price, secondCandy.price);
    }

    public static int compareByWeight(Candy firstCandy, Candy secondCandy) {
        return Long.compare(firstCandy.weight, secondCandy.weight);
    }

    public static int compareBySugarContent(Candy firstCandy, Candy secondCandy) {
        return Double.compare(firstCandy.sugarContent, secondCandy.sugarContent);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Candy candy = (Candy) o;
        return name.equals(candy.name);
    }

    @Override
    public String toString() {
        return "Candy{" +
                "name='" + name + '\'' +
                ", price=" + price +
                ", weight=" + weight +
                ", sugarContent=" + sugarContent +
                '}';
    }
}