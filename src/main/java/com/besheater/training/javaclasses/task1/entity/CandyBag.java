package com.besheater.training.javaclasses.task1.entity;

import java.util.*;

public class CandyBag {
    private Set<Candy> candies;

    public CandyBag() {
        this.candies = new HashSet<>();
    }

    public CandyBag(Set<? extends Candy> candies) {
        checkCandies(candies);
        this.candies = new HashSet<>(candies);
    }

    private void checkCandies(Collection<? extends Candy> candies) {
        if (candies == null) {
            throw new IllegalArgumentException("Candies collection cannot be null");
        }
    }

    private void checkCandy(Candy candy) {
        if (candy == null) {
            throw new IllegalArgumentException("Candy cannot be null");
        }
    }

    public Set<Candy> getAll() {
        return Collections.unmodifiableSet(candies);
    }

    public boolean add(Candy candy) {
        checkCandy(candy);
        return candies.add(candy);
    }

    public boolean add(Collection<? extends Candy> candies) {
        checkCandies(candies);
        return this.candies.addAll(candies);
    }

    public boolean remove(Candy candy) {
        checkCandy(candy);
        return candies.remove(candy);
    }

    public void sort(Comparator<Candy> comparator) {
        // Chain additional comparator that fully consistent with candy .equals() contract.
        // It ensures that no element will be removed during sorting.
        Comparator<Candy> safeComparator = comparator.thenComparing(Candy::compareByName);
        Set<Candy> sortedCandies = new TreeSet<>(safeComparator);
        sortedCandies.addAll(candies);
        candies = sortedCandies;
    }

    public long getPrice() {
        return candies.stream().mapToLong(Candy::getPrice).sum();
    }

    public long getWeight() {
        return candies.stream().mapToLong(Candy::getWeight).sum();
    }

    public String printContent() {
        StringBuilder builder = new StringBuilder();
        String prefix =    "***************************************************\n";
        String suffix =    "***************************************************";
        String separator = "---------------------------------------------------\n";
        builder.append(prefix);
        builder.append(String.format("%12s%12s%12s%12s\n",
                                     "Name", "Sugar, %", "Weight, g", "Price, ₸"));
        builder.append(separator);
        for (Candy candy : candies) {
            builder.append(String.format("%12s", candy.getName()));
            builder.append(String.format("%12.0f", candy.getSugarContent() * 100));
            builder.append(String.format("%12d", candy.getWeight()));
            builder.append(String.format("%12d", candy.getPrice()));
            builder.append("\n");
        }
        builder.append(suffix);
        return builder.toString();
    }

    @Override
    public String toString() {
        return "CandyBag{" +
                "candies=" + candies +
                '}';
    }
}