package com.besheater.training.javaclasses.task1.repository;

import com.besheater.training.javaclasses.task1.entity.Candy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CandyRepository {
    private Map<String, Candy> candies = new HashMap<>();

    public CandyRepository() {
        add(new Candy("Snickers", 269, 95, 0.47));
        add(new Candy("Mars", 179, 50, 0.46));
        add(new Candy("Bounty", 179, 55, 0.48));
        add(new Candy("M&M's", 299, 70, 0.65));
        add(new Candy("Skittles", 370, 100, 0.75));
        add(new Candy("Twix", 161, 55, 0.48));
        add(new Candy("Kit Kat", 144, 40, 0.49));
        add(new Candy("Oreo", 262, 95, 0.42));
        add(new Candy("Meller", 215, 38, 0.71));
        add(new Candy("Milky Way", 100, 26, 0.60));
        add(new Candy("Picnic", 242, 76, 0.46));
    }

    public List<Candy> getAll() {
        return new ArrayList<>(candies.values());
    }

    public Candy get(String name) {
        return candies.get(name);
    }

    private void add(Candy candy) {
        candies.put(candy.getName(), candy);
    }
}