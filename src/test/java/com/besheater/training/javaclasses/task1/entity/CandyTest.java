package com.besheater.training.javaclasses.task1.entity;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CandyTest {

    @Test
    void constructor_nullArguments_throwsException() {
        assertThrows(IllegalArgumentException.class,
                () -> new Candy(null, 100, 50, 0.3));
    }

    @Test
    void constructor_invalidName_throwsException() {
        assertThrows(IllegalArgumentException.class,
                () -> new Candy("", 100, 50, 0.3));
    }

    @Test
    void constructor_invalidPrice_throwsException() {
        assertThrows(IllegalArgumentException.class,
                () -> new Candy("Sugar", -1, 50, 0.3));
    }

    @Test
    void constructor_invalidWeight_throwsException() {
        assertThrows(IllegalArgumentException.class,
                () -> new Candy("Sugar", 100, -2, 0.3));
    }

    @Test
    void constructor_invalidSugarContent_throwsException() {
        assertThrows(IllegalArgumentException.class,
                () -> new Candy("Sugar", 100, 30, -0.001));
        assertThrows(IllegalArgumentException.class,
                () -> new Candy("Sugar", 100, 30, Candy.MIN_SUGAR_CONTENT - 0.0001));
        assertThrows(IllegalArgumentException.class,
                () -> new Candy("Sugar", 100, 30, Candy.MAX_SUGAR_CONTENT + 0.0001));
        assertThrows(IllegalArgumentException.class,
                () -> new Candy("Sugar", 100, 30, 100));
    }

    @Test
    void isSugarContentInRange_invalidRange_throwsException() {
        Candy candy = new Candy("Sugar", 100, 55, 0.5);
        assertThrows(IllegalArgumentException.class,
                () -> candy.isSugarContentInRange(0.50, 0.49));
        assertThrows(IllegalArgumentException.class,
                () -> candy.isSugarContentInRange(-0.1, 0.5));
        assertThrows(IllegalArgumentException.class,
                () -> candy.isSugarContentInRange(0.5,-0.1));
        assertThrows(IllegalArgumentException.class,
                () -> candy.isSugarContentInRange(-0.3,-0.1));
        assertThrows(IllegalArgumentException.class,
                () -> candy.isSugarContentInRange(0.2, Candy.MAX_SUGAR_CONTENT + 0.0001));
        assertThrows(IllegalArgumentException.class,
                () -> candy.isSugarContentInRange(Candy.MIN_SUGAR_CONTENT - 0.0001, 0.6));
    }

    @Test
    void isSugarContentInRange_validRange_returnsWhatExpected() {
        Candy candy = new Candy("Sugar", 100, 55, 0.5);
        assertTrue(candy.isSugarContentInRange(0.49, 0.51));
        assertTrue(candy.isSugarContentInRange(0.5, 0.53));
        assertTrue(candy.isSugarContentInRange(0.47, 0.5));
        assertTrue(candy.isSugarContentInRange(0.5, 0.5));
        assertTrue(candy.isSugarContentInRange(0.001, 0.999));
        assertTrue(candy.isSugarContentInRange(Candy.MIN_SUGAR_CONTENT, Candy.MAX_SUGAR_CONTENT));

        assertFalse(candy.isSugarContentInRange(0.501, 1));
        assertFalse(candy.isSugarContentInRange(0.3, 0.49999));
        assertFalse(candy.isSugarContentInRange(0.8, 0.99));
    }

    @Test
    void compareByName_nullArguments_throwException() {
        Candy candy = new Candy("Sugar", 100, 55, 0.5);
        assertThrows(NullPointerException.class, () -> Candy.compareByName(null, candy));
        assertThrows(NullPointerException.class, () -> Candy.compareByName(candy, null));
        assertThrows(NullPointerException.class, () -> Candy.compareByName(null, null));
    }

    @Test
    void compareByName_sampleCandies_returnsWhatExpected() {
        Candy candyA = new Candy("aCandy", 100, 55, 0.5);
        Candy candyB = new Candy("bCandy", 100, 55, 0.5);
        assertTrue(Candy.compareByName(candyB, candyA) > 0);
        assertTrue(Candy.compareByName(candyA, candyB) < 0 );
        assertTrue(Candy.compareByName(candyA, candyA) == 0);
        assertTrue(Candy.compareByName(candyB, candyB) == 0);
    }

    @Test
    void compareByPrice_nullArguments_throwException() {
        Candy candy = new Candy("Sugar", 100, 55, 0.5);
        assertThrows(NullPointerException.class, () -> Candy.compareByPrice(null, candy));
        assertThrows(NullPointerException.class, () -> Candy.compareByPrice(candy, null));
        assertThrows(NullPointerException.class, () -> Candy.compareByPrice(null, null));
    }

    @Test
    void compareByPrice_sampleCandies_returnsWhatExpected() {
        Candy candyA = new Candy("Candy", 50, 55, 0.5);
        Candy candyB = new Candy("Candy", 70, 55, 0.5);
        assertTrue(Candy.compareByPrice(candyB, candyA) > 0);
        assertTrue(Candy.compareByPrice(candyA, candyB) < 0 );
        assertTrue(Candy.compareByPrice(candyA, candyA) == 0);
        assertTrue(Candy.compareByPrice(candyB, candyB) == 0);
    }

    @Test
    void compareByWeight_nullArguments_throwException() {
        Candy candy = new Candy("Sugar", 100, 55, 0.5);
        assertThrows(NullPointerException.class, () -> Candy.compareByWeight(null, candy));
        assertThrows(NullPointerException.class, () -> Candy.compareByWeight(candy, null));
        assertThrows(NullPointerException.class, () -> Candy.compareByWeight(null, null));
    }

    @Test
    void compareByWeight_sampleCandies_returnsWhatExpected() {
        Candy candyA = new Candy("Candy", 65, 30, 0.7);
        Candy candyB = new Candy("Candy", 65, 32, 0.7);
        assertTrue(Candy.compareByWeight(candyB, candyA) > 0);
        assertTrue(Candy.compareByWeight(candyA, candyB) < 0 );
        assertTrue(Candy.compareByWeight(candyA, candyA) == 0);
        assertTrue(Candy.compareByWeight(candyB, candyB) == 0);
    }

    @Test
    void compareBySugarContent_nullArguments_throwException() {
        Candy candy = new Candy("Sugar", 100, 55, 0.5);
        assertThrows(NullPointerException.class, () -> Candy.compareBySugarContent(null, candy));
        assertThrows(NullPointerException.class, () -> Candy.compareBySugarContent(candy, null));
        assertThrows(NullPointerException.class, () -> Candy.compareBySugarContent(null, null));
    }

    @Test
    void compareBySugarContent_sampleCandies_returnsWhatExpected() {
        Candy candyA = new Candy("Candy", 65, 78, 0.61);
        Candy candyB = new Candy("Candy", 65, 78, 0.65);
        assertTrue(Candy.compareBySugarContent(candyB, candyA) > 0);
        assertTrue(Candy.compareBySugarContent(candyA, candyB) < 0 );
        assertTrue(Candy.compareBySugarContent(candyA, candyA) == 0);
        assertTrue(Candy.compareBySugarContent(candyB, candyB) == 0);
    }
}