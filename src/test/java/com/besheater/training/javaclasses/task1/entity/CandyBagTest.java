package com.besheater.training.javaclasses.task1.entity;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class CandyBagTest {
    private Candy candy;
    private Set<Candy> candies;

    @BeforeEach
    void makeCandy() {
        candy = new Candy("Candy", 80, 45, 0.38);
    }

    @BeforeEach
    void makeCandies() {
        candies = new HashSet<>();
        candies.add(new Candy("Sugar", 100, 50, 1));
        candies.add(new Candy("Margo", 45, 80, 0.48));
        candies.add(new Candy("Ipsum", 11, 200, 0.25));
        candies.add(new Candy("Abc", 1001, 38, 0.78));
        candies.add(new Candy("1", 1, 1, 0));
    }

    @Test
    void constructor_nullArguments_throwsException() {
        assertThrows(IllegalArgumentException.class, () -> new CandyBag(null));
    }

    @Test
    void getAll_attemptToModifyReturnedCollection_throwsException() {
        CandyBag candyBag = new CandyBag();
        assertThrows(UnsupportedOperationException.class,
                () -> candyBag.getAll().add(candy));
        assertThrows(UnsupportedOperationException.class,
                () -> candyBag.getAll().addAll(candies));
        assertThrows(UnsupportedOperationException.class,
                () -> candyBag.getAll().remove(candy));
        assertThrows(UnsupportedOperationException.class,
                () -> candyBag.getAll().removeAll(candies));
    }

    @Test
    void getAll_emptyCandyBag_returnsEmptyCollection() {
        CandyBag candyBag = new CandyBag();
        assertEquals(Collections.emptySet(), candyBag.getAll());
    }

    @Test
    void getAll_fullCandyBag_returnsAllCandies() {
        CandyBag candyBag = new CandyBag();
        candyBag.add(candies);
        assertEquals(candies, candyBag.getAll());
        candyBag = new CandyBag(candies);
        assertEquals(candies, candyBag.getAll());
        candyBag = new CandyBag();
        candyBag.add(candy);
        assertEquals(new HashSet<>(Arrays.asList(candy)), candyBag.getAll());
    }

    @Test
    void add_nullArguments_throwsException() {
        CandyBag candyBag = new CandyBag();
        Candy candy = null;
        assertThrows(IllegalArgumentException.class, () -> candyBag.add(candy));
        List<Candy> candies = null;
        assertThrows(IllegalArgumentException.class, () -> candyBag.add(candies));
    }

    @Test
    void add_newCandies_returnsTrueAndaddsToCandyBag() {
        CandyBag candyBag = new CandyBag();
        assertTrue(candyBag.add(candies));
        assertEquals(candies, candyBag.getAll());
        candyBag = new CandyBag();
        Candy candyA = new Candy("Summer", 78, 15, 0.8);
        Candy candyB = new Candy("96", 42, 42, 0);
        assertTrue(candyBag.add(candyA));
        assertTrue(candyBag.add(candyB));
        assertEquals(new HashSet<>(Arrays.asList(candyA, candyB)), candyBag.getAll());
    }

    @Test
    void add_tryToAddAlreadyAddedCandies_returnsFalse() {
        CandyBag candyBag = new CandyBag();
        candyBag.add(candies);
        assertFalse(candyBag.add(candies));
        candyBag = new CandyBag();
        candyBag.add(candy);
        assertFalse(candyBag.add(candy));
    }

    @Test
    void remove_nullArguments_throwsException() {
        CandyBag candyBag = new CandyBag();
        assertThrows(IllegalArgumentException.class, () -> candyBag.remove(null));
    }

    @Test
    void remove_candiesThatExistInCandyBag_returnsTrueAndRemovesCandie() {
        CandyBag candyBag = new CandyBag();
        candyBag.add(candy);
        assertTrue(candyBag.remove(candy));
        assertEquals(Collections.emptySet(), candyBag.getAll());
        candyBag = new CandyBag();
        candyBag.add(candies);
        candyBag.add(candy);
        assertTrue(candyBag.remove(candy));
        assertEquals(candies, candyBag.getAll());
    }

    @Test
    void remove_candiesThatDontExistInCandyBag_returnsFalse() {
        CandyBag candyBag = new CandyBag();
        candyBag.add(new Candy("Hardy", 42, 10, 0.001));
        assertFalse(candyBag.remove(candy));
        candyBag = new CandyBag();
        candyBag.add(candies);
        assertFalse(candyBag.remove(candy));
    }

    @Test
    void sort_nullArguments_throwsException() {
        CandyBag candyBag = new CandyBag();
        assertThrows(NullPointerException.class, () -> candyBag.sort(null));
    }

    @Test
    void sort_nameComparator_sortsAsExpected() {
        CandyBag candyBag = new CandyBag();
        Candy candyA = new Candy("Acandy", 100, 12, 0.3);
        Candy candyB = new Candy("Bcandy", 100, 12, 0.3);
        Candy candyC = new Candy("Ccandy", 100, 12, 0.3);
        Candy candyD = new Candy("Dcandy", 100, 12, 0.3);
        Candy candyE = new Candy("Ecandy", 100, 12, 0.3);
        List<Candy> candies = Arrays.asList(candyA, candyB, candyC, candyD, candyE);

        candyBag.add(Arrays.asList(candyE, candyC, candyA, candyD, candyB));
        candyBag.sort(Candy::compareByName);
        assertEquals(candies, new ArrayList<>(candyBag.getAll()));
    }

    @Test
    void sort_weightComparator_sortsAsExpected() {
        CandyBag candyBag = new CandyBag();
        Candy candyA = new Candy("Acandy", 100, 10, 0.3);
        Candy candyB = new Candy("Bcandy", 100, 15, 0.3);
        Candy candyC = new Candy("Ccandy", 100, 15, 0.3);
        Candy candyD = new Candy("Dcandy", 100, 24, 0.3);
        Candy candyE = new Candy("Ecandy", 100, 89, 0.3);
        List<Candy> candies = Arrays.asList(candyA, candyB, candyC, candyD, candyE);

        candyBag.add(Arrays.asList(candyE, candyC, candyA, candyD, candyB));
        candyBag.sort(Candy::compareByWeight);
        assertEquals(candies, new ArrayList<>(candyBag.getAll()));
    }

    @Test
    void sort_priceComparator_sortsAsExpected() {
        CandyBag candyBag = new CandyBag();
        Candy candyA = new Candy("Acandy", 100, 10, 0.3);
        Candy candyB = new Candy("Bcandy", 125, 10, 0.3);
        Candy candyC = new Candy("Ccandy", 200, 10, 0.3);
        Candy candyD = new Candy("Dcandy", 200, 10, 0.3);
        Candy candyE = new Candy("Ecandy", 555, 10, 0.3);
        List<Candy> candies = Arrays.asList(candyA, candyB, candyC, candyD, candyE);

        candyBag.add(Arrays.asList(candyE, candyC, candyA, candyD, candyB));
        candyBag.sort(Candy::compareByPrice);
        assertEquals(candies, new ArrayList<>(candyBag.getAll()));
    }

    @Test
    void sort_sugarContentComparator_sortsAsExpected() {
        CandyBag candyBag = new CandyBag();
        Candy candyA = new Candy("Acandy", 100, 10, 0.1);
        Candy candyB = new Candy("Bcandy", 100, 10, 0.1);
        Candy candyC = new Candy("Ccandy", 100, 10, 0.1);
        Candy candyD = new Candy("Dcandy", 100, 10, 0.6);
        Candy candyE = new Candy("Ecandy", 100, 10, 1.0);
        List<Candy> candies = Arrays.asList(candyA, candyB, candyC, candyD, candyE);

        candyBag.add(Arrays.asList(candyE, candyC, candyA, candyD, candyB));
        candyBag.sort(Candy::compareBySugarContent);
        assertEquals(candies, new ArrayList<>(candyBag.getAll()));
    }

    @Test
    void getPrice_emptyBag_returns0() {
        CandyBag candyBag = new CandyBag();
        assertEquals(0, candyBag.getPrice());
    }

    @Test
    void getPrice_sampleBagWithCandies1_returnsTotalPrice() {
        CandyBag candyBag = new CandyBag();
        Candy candyA = new Candy("Acandy", 10, 10, 0.5);
        Candy candyB = new Candy("Bcandy", 10, 10, 0.5);
        Candy candyC = new Candy("Ccandy", 0, 10, 0.5);
        Candy candyD = new Candy("Dcandy", 5, 10, 0.5);
        Candy candyE = new Candy("Ecandy", 1, 10, 0.5);
        candyBag.add(Arrays.asList(candyA, candyB, candyC, candyD, candyE));
        assertEquals(26, candyBag.getPrice());
    }

    @Test
    void getPrice_sampleBagWithCandies2_returnsTotalPrice() {
        CandyBag candyBag = new CandyBag();
        Candy candyA = new Candy("Acandy", 0, 10, 0.1);
        Candy candyB = new Candy("Bcandy", 0, 10, 0.1);
        Candy candyC = new Candy("Ccandy", 0, 10, 0.1);
        candyBag.add(Arrays.asList(candyA, candyB, candyC));
        assertEquals(0, candyBag.getPrice());
    }


    @Test
    void getWeight_emptyBag_returns0() {
        CandyBag candyBag = new CandyBag();
        assertEquals(0, candyBag.getWeight());
    }

    @Test
    void getWeight_sampleBagWithCandies1_returnsTotalWeight() {
        CandyBag candyBag = new CandyBag();
        Candy candyA = new Candy("Acandy", 100, 1, 0.5);
        Candy candyB = new Candy("Bcandy", 100, 2, 0.5);
        Candy candyC = new Candy("Ccandy", 100, 0, 0.5);
        Candy candyD = new Candy("Dcandy", 100, 0, 0.5);
        Candy candyE = new Candy("Ecandy", 100, 30, 0.5);
        candyBag.add(Arrays.asList(candyA, candyB, candyC, candyD, candyE));
        assertEquals(33, candyBag.getWeight());
    }

    @Test
    void getWeight_sampleBagWithCandies2_returnsTotalWeight() {
        CandyBag candyBag = new CandyBag();
        Candy candyA = new Candy("Acandy", 100, 0, 0.1);
        Candy candyB = new Candy("Bcandy", 100, 0, 0.1);
        Candy candyC = new Candy("Ccandy", 100, 0, 0.1);
        candyBag.add(Arrays.asList(candyA, candyB, candyC));
        assertEquals(0, candyBag.getWeight());
    }

    @Test
    void printContent_emptyBag_printsOnlyTableTitle() {
        CandyBag candyBag = new CandyBag();
        String expected = "***************************************************\n" +
                          "        Name    Sugar, %   Weight, g    Price, ₸\n"    +
                          "---------------------------------------------------\n" +
                          "***************************************************";

        assertEquals(expected, candyBag.printContent());
    }

    @Test
    void printContent_sampleCandyBag_printsAllContentAccordingly() {
        CandyBag candyBag = new CandyBag();
        candyBag.add(new Candy("Astra", 55, 48, 0.41));
        candyBag.add(new Candy("Bella", 100, 95, 0.78));
        candyBag.add(new Candy("The one", 243, 100, 1));
        candyBag.add(new Candy("3", 124, 100, 0));
        candyBag.add(new Candy("Click", 500, 210, 0.23));

        candyBag.sort(Candy::compareByWeight);
        String expected = "***************************************************\n" +
                          "        Name    Sugar, %   Weight, g    Price, ₸\n"    +
                          "---------------------------------------------------\n" +
                          "       Astra          41          48          55\n" +
                          "       Bella          78          95         100\n" +
                          "           3           0         100         124\n" +
                          "     The one         100         100         243\n" +
                          "       Click          23         210         500\n" +
                          "***************************************************";
        assertEquals(expected, candyBag.printContent());
    }
}